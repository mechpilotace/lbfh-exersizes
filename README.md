# Linux Basics for Hackers - End of Chapter Projects
The following are the End of Chapter Exersizes that the book lays out for you. They assume that you have a working Kali Linux environment.
I do not own this content and am using this for learning/pracitce purposes. Please support the Author and No Starch Press.

## Chapter 1
1. Use the ``ls`` command from the root directory to explore the directory structure of Linux. Move to each fo the directories with the ``cd`` command.
1. Use the ``whoami`` command to verify which user you are loggedn in as.
1. Use the ``locate`` command to find wordlists that can be used for password cracking.
1. Use the ``cat`` command to create a new file and then append to that file. Keep in mind that ``>`` redirects input to a file and ``>>`` appends to a file.
1. Create a new directory called *hackerdirectory* and create a new file in that direcore name *hackedfile*. Now copy that file to your */root* directory and rename it *secretfile*.

## Chapter 2
1. Navigate to ``/usr/share/wordlists/metasploit``. This is a directory of multiple wordlists that can be used to brute forced passwords in various password-protected devices using Metasploit, the most popular pentesting and hacking framework.
1. Use the ``cat`` command to view the contents of the file *passwords.lst*.
1. Use the ``more`` command to display the file *passwords.lst*.
1. Use the ``less`` command to view the file *passwords.lst*.
1. Now use the ``nl`` command to place line number son the passwords in the *passwords.lst*. There should be 88,396 passwords.
1. Use the ``tail`` command to see the last 20 passwords in *passwords.lst*.
1. Use the ``cat`` command to display passwords.lst and pip it ot find all the passwords that contain *123*.

## Chapter 3
1. Find information on your active network interfaces.
1. Change the IP address on **eth0** to 192.168.1.1
1. Change your hardware address on **eth0**.
1. Check whether you have any available wireless interfaces active.
1. Reset your IP address to a DHCP-assigned address.
1. Find the nameserver and email server of your favorite website.
1. Add Google's DNS server to your */etc/resolv.conf* file so your stream refers to that server when it can't resolve a domain name query with your local DNS server.

## Chapter 4
1. Install a new software package from the Kali repository.
1. Remove that same software packate.
1. Update your repository.
1. Upgrade your software packages.
1. Select a new piece of software from github and clone it to your system.

## Chapter 5
1. Select a directory and run and long listing on it. Note the permissions on the files and directories.
1. Select a file you don't have permission to execute and give yourself execute permissions using the ``chmod`` command. Try using both the numeral method (777) and the UGO method.
1. Choose another file and change its ownership using chown.
1. Use the ``find``command to fine all files with the SGID bit set.

## Chapter 6
1. Run the ``ps`` command with the ``aux`` option on your system and note which process is first and which is last.
1. Run the ``top`` command and note the two processes using the greates amount of your resources.
1. Use the ``kill`` command to kill the process that uses the most resources.
1. Use the ``renice``  command to reduce the priorityh of a running process to +19.
1. Create a script called **myscannnig** (the content is not important) with a text editor and then schedulit to run next Wednesday at 1 AM.

## Chapter 7
1. View all of your environment variables with the ``more`` command.
1. Use the ``echo`` command to view the ``HOSTNAME`` variable.
1. Find a method to chang the slash (/) to a backslash (\) in the faux Microsoft cmd PS1 example (see listing 7-2).
1. Create a variable name **MYNEWVARIABLE** and put your name in it.
1. Use ``echo`` to vie the contents of **MYNEWVARIABLE**.
1. Export **MYNEWVARIABLE** so that it's available in all environments.
1. Use the ``echo`` command to view the contents of the ``PATH`` variable.
1. Add your home directory to the ``PATH`` variable so that any binaries in your home directory can be used in any directory.
1. Change your ``PS1`` variable to "World's Greatest Hacker:".

## Chapter 8
1. Create your own greeting script similar to our *HelloHackersArise* script.
1. Create a script similar to *MySQLscanner.sh* but design it to find systems with Microsoft's SQL server database at port 1433. Call it MSSQLscanner.
1. Alter that *MSSQLsanner* script to prompt the user fo a staring and enign IP address and the por tto search for. Then filter out all the IP addresses where those ports are closed and display only those that are open.

## Chapter 9
1. Create three scripts to combine, similar to what we did in Chapter 8.
Name them *Linux4Hackers1*, *Linux4Hackers2*, and *Linux4Hackers3*.
1. Create a tarball from these three files. Name the tarball *L4H*. Note how the sum of the three files changese when they are tarred together.
1. Compress the *L4H* tarball with gzip. Note how the size of the files changes.
1. Investigate how you can control overwriteing existing files. Now uncompress the *L4H* file.
1. Repeat exersize 3 using both bzip2 and compress.
1. Make a physical, bit-by-bit copy of one of your flash drives using the ``dd`` command.

## Chapter 10
1. Use the ``mount`` and ``unmount`` commands to mount and unmount your flash drive.
1. Check the ammount of disk space free on your primary hard drive
1. Check for errors on your flash drive with ``fsck``.
1. Use the ``dd`` command to copy the entire contents of one flash drive to another, including deleted files.
1. Use the ``lsblk`` command to determine the basic characteristices of your block devices.

## Chapter 11
1. Use the ``locate`` command to find all the ``rsyslog`` files.
1. Open the *rsyslog.conf* file and change your log rotation to one week.
1. Disable logging on your system. Investigate what is logged in the file ``/var/log/syslog`` when you disable logging.
1. Use the ``shred`` command to shred and delete all your ``kern`` log files.

## Chapter 12
1. Start your apache2 service through the command line.
1. Using the *index.html* file, create a simple website announcing your arrival in to the exciting world of hacking.
1. Start your SSH service via the command line. Now connect to your Kali system from another system on your LAN.
1. Start your MySQL database service and change the root password to *hackers-arise*. Change the ``mysql`` database.
1. Start your PostgreSQL database service. Set it up as described in this chapter to be used by Metasploit.

## Chapter 13
1. Run ``traceroute`` to your faoverite website. How many hops appear between you and your favorite site?
1. Download and install the Tor browser. Now, browse anonymously around the web just as your would with any other browser and see if you notice any difference in speed.
1. Try using ``proxychains`` with the Firefox browser to navigate to your favorite website.
1. Explore commmercial VPN services from some of the vendors listed in thei chapter. Choos one and test a free trial.
1. Open a free ProtonMail account and send a secure greeting to *occupytheweb@protonmail.com*.

## Chapter 14
1. Check your network devices with ``ifconfig``. Note any wireless extensions.
1. Run ``iwconfig`` and note any wireless network adapters.
1. Check to see what Wi-Fi APs are in range with ``iwlist``.
1. Check to see what Wi-Fi APs are in range with ``nmcli``. Which do find more useful and intuitive, nmcli or iwlist?
1. Conect to your Wi-Fi AP using ``nmcli``.
1. Bring up your Bluetooth adapter with ``hciconfig`` and scan for nearby discoverable Bluetooth devices with ``hcitool``.
1. Test whether those Bluetooth devices are within reachable distance with ``l2ping``.

## Chapter 15
1. Check the version of your kernel.
1. List the modules in your kernel.
1. Enable IP forwarding with a ``sysctl`` command.
1. Edit your */etc/sysctl.conf* file to enable IP forwarding. Now, disable IP forwarding.
1. Select one kernel module and learn more about it with ``modinfo``.

## Chapter 16
1. Schedule your *MySQLscanner.sh* script to run every Wednesday at 3 PM.
1. Schedule your *MySQLscanner.sh* script to run every 10th day of the month in April, June, and August.
1. Schedule your *MySQLscanner.sh* script to run every Tueday through Thursday at 10 AM.
1. Schedule your *MySQLscanner.sh* script to run daily at noon using the shortcuts.
1. Update your *rc.d* script to run PostgreSQL every time your system boots.
1. Download and install rcconf and ad the PostgreSQL and MySQL databases to start a bootup.

## Chapter 17
1. Build the SSH banner-grabing tool from Listing 17-5 and then edit it to do a banner grab on port 21.
1. Rather than hardcoding the IP address into the script, edi your banner-grabbing tool so that it prompts the user for the IP address.
1. Edit your *tcp_server.py* to prompt the user for the port to listen on.
1. Build the FTPcraker in Listing 17-7 and then edit it to use a wordlist for user variable (similar to what we did with the password) rather than prompting the user for input.
1. Add an exept clause to the banner-grabbing tool that prints "no answer" if the port is closed.